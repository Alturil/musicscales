using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xunit;

namespace IntegrationTests
{
    public class ApiIntegrationTests
    {
        private HttpClient _apiServer;

        public ApiIntegrationTests()
        {
            _apiServer = new ApiServer().Client;            
        }
        
        [Fact]
        public async Task GetAllScales()
        {
            // Arrange
            

            // Act
            var getAllScalesResponse = await _apiServer.GetAsync("/scale");

            // Assert
            Assert.NotNull(getAllScalesResponse);
        }

        [Fact]
        public async Task CreatePentatonicScale()
        {
            // Arrange
            var pentatonicText = File.ReadAllText(@"ApiTestRequests/MajorPentatonicScale.json");
            var stringContent = new StringContent(pentatonicText, Encoding.UTF8, "application/json");

            // Act
            var response = await _apiServer.PostAsync("/scale", stringContent);
            var result = JObject.Parse(await response.Content.ReadAsStringAsync());

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

    }
}
