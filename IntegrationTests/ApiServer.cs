using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using musicscales;

namespace IntegrationTests
{
    public class ApiServer
    {
        public HttpClient Client;

        public ApiServer()
        {
            var testServer = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
            );

            Client = testServer.CreateClient();
        }
    }
}
