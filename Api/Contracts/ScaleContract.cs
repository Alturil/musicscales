using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using LiteDB;

namespace musicscales.Api.Contracts
{
    [DataContract]
    public class ScaleContract
    {
        [BsonId]
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public ScaleMetadataContract Metadata { get; set; }
        [DataMember]
        public IList<IntervalContract> Intervals { get; set; }
    }
}
