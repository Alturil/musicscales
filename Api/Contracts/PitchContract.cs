using System.Runtime.Serialization;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Contracts
{
    [DataContract]
    public class PitchContract
    {
        [DataMember]
        public DiatonicPitchName Name { get; set; }
        [DataMember]
        public AccidentalName Accidental { get; set; }
    }
}
