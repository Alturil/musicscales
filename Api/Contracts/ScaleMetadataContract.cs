using System.Collections.Generic;
using System.Runtime.Serialization;

namespace musicscales.Api.Contracts
{
    [DataContract]
    public class ScaleMetadataContract
    {
        [DataMember]
        public IList<string> Names { get; set; }
    }
}
