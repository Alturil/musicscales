using System.Runtime.Serialization;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Contracts
{
    [DataContract]
    public class IntervalContract
    {
        [DataMember]
        public IntervalSizeName Name { get; set; }
        [DataMember]
        public IntervalQualityName Quality { get; set; }
    }
}
