using System.Collections.Generic;
using System.Runtime.Serialization;

namespace musicscales.Api.Contracts.Responses
{
    [DataContract]
    public class PitchCollectionContract
    {
        [DataMember]
        public PitchContract Root { get; set; }
        [DataMember]
        public string ScaleName { get; set; }
        [DataMember]
        public IList<PitchContract> Pitches { get; set; }
    }
}
