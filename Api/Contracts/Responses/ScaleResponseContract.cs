using System.Runtime.Serialization;

namespace musicscales.Api.Contracts.Responses
{
    [DataContract]
    public class ScaleResponseContract
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public ScaleContract Scale { get; set; }
    }
}
