using System;
using System.Runtime.Serialization;

namespace musicscales.Api.Contracts.Responses
{
    [DataContract]
    public class GuidResponseContract
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Guid Guid { get; set; }
    }
}
