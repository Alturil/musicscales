using System.Collections.Generic;
using System.Runtime.Serialization;

namespace musicscales.Api.Contracts.Responses
{
    [DataContract]
    public class ScaleCollectionResponseContract
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public IList<ScaleContract> Scales { get; set; }
    }
}
