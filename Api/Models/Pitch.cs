using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// A musical pitch with diatonic name, accidental, pitch offset and semitone offset
    /// </summary>
    public class Pitch
    {
        public DiatonicPitchName Name { get; set; }
        public AccidentalName Accidental { get; set; }
        /// <summary>
        /// Number of pitches offset added by the pitch
        /// </summary>
        public int PitchOffset { get; set; }
        /// <summary>
        /// Number of semitones offset added by the pitch
        /// </summary>
        public int SemitoneOffset { get; set; }
    }
}
