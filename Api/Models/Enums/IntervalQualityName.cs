namespace musicscales.Api.Models.Enums
{
    /// <summary>
    /// Interval quality name
    /// </summary>
    public enum IntervalQualityName
    {
        Diminished,
        Perfect,
        Augmented,
        Minor,
        Major
    }
}
