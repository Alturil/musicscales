namespace musicscales.Api.Models.Enums
{
    /// <summary>
    /// Interval size name
    /// </summary>
    public enum IntervalSizeName
    {
        Unison,
        Second,
        Third,
        Fourth,
        Fifth,
        Sixth,
        Seventh,
        Octave
    }
}
