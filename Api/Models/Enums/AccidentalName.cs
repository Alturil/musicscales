namespace musicscales.Api.Models.Enums
{
    /// <summary>
    /// Accidental name
    /// </summary>
    public enum AccidentalName
    {
        DoubleFlat,
        Flat,
        Natural,
        Sharp,
        DoubleSharp
    }
}
