namespace musicscales.Api.Models.Enums
{
    /// <summary>
    /// Diatonic pitch name
    /// </summary>
    public enum DiatonicPitchName
    {
        C,
        D,
        E,
        F,
        G,
        A,
        B
    }
}
