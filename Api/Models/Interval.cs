using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// An interval with size, quality, interval offset and semitone offset
    /// </summary>
    public class Interval
    {
        public IntervalSizeName Name { get; set; }
        public IntervalQualityName Quality { get; set; }
        /// <summary>
        /// Number of pitches offset added by the interval
        /// </summary>
        public int PitchOffset { get; set; }
        /// <summary>
        /// Number of semitones offset added by the interval
        /// </summary>
        public int SemitoneOffset { get; set; }
    }
}
