using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// Size of the interval
    /// </summary>
    public class IntervalSize
    {
        public IntervalSizeName Name { get; set; }
        /// <summary>
        /// Number of pitches offset added by the interval size
        /// </summary>
        public int PitchOffset { get; set; }
    }
}
