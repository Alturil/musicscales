using System.Collections.Generic;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// A collection of Intervals
    /// </summary>
    public class IntervalCollection
    {
        public IntervalSizeName Name { get; set; }
        /// <summary>
        /// Number of pitches offset added by the interval
        /// </summary>
        public int PitchOffset { get; set; }
        public IList<IntervalQuality> Quality { get; set; }
    }
}
