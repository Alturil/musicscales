using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// An accidental with name and semitone offset
    /// </summary>
    public class Accidental
    {
        public AccidentalName Name { get; set; }
        /// <summary>
        /// Number of semitones offset added by the accidental
        /// </summary>
        public int SemitoneOffset { get; set; }
    }
}
