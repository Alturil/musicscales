using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// A pitch with name, pitch offset and semitone offset
    /// </summary>
    public class DiatonicPitch
    {
        public DiatonicPitchName Name { get; set; }
        /// <summary>
        /// Number of pitches offset added by the pitch
        /// </summary>
        public int PitchOffset { get; set; }
        /// <summary>
        /// Number of semitones offset added by the pitch
        /// </summary>
        public int SemitoneOffset { get; set; }
    }
}
