using musicscales.Api.Models.Enums;

namespace musicscales.Api.Models
{
    /// <summary>
    /// Interval quality
    /// </summary>
    public class IntervalQuality
    {
        public IntervalQualityName Name { get; set; }
        /// <summary>
        /// Number of semitones offset added by the interval
        /// </summary>
        public int SemitoneOffset { get; set; }
    }
}
