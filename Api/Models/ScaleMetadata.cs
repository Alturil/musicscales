using System.Collections.Generic;

namespace musicscales.Api.Models
{
    /// <summary>
    /// Scale metadata
    /// </summary>
    public class ScaleMetadata
    {
        /// <summary>
        /// Name(s) of the scale
        /// </summary>
        public IList<string> Names { get; set; }
    }
}
