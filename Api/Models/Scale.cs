using System;
using System.Collections.Generic;
using LiteDB;

namespace musicscales.Api.Models
{
    /// <summary>
    /// A musical scale
    /// </summary>
    public class Scale
    {
        /// <summary>
        /// Guid of the scale
        /// </summary>
        [BsonId] // TODO: Having this here is weird
        public Guid Guid { get; set; }
        public ScaleMetadata Metadata { get; set; }
        public IList<Interval> Intervals { get; set; }
    }
}
