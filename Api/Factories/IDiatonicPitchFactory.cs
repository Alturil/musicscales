using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public interface IDiatonicPitchFactory
    {
        DiatonicPitch GetDiatonicPitch(DiatonicPitchName name);
        DiatonicPitch GetDiatonicPitch(int pitchOffset);
        IList<DiatonicPitch> GetAllDiatonicPitches();
    }
}
