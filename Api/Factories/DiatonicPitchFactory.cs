using System.Collections.Generic;
using System.Linq;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public class DiatonicPitchFactory : IDiatonicPitchFactory
    {
        private static List<DiatonicPitch> AllDiatonicPitches => new List<DiatonicPitch>
        {
            new DiatonicPitch() { Name = DiatonicPitchName.C, PitchOffset = 0, SemitoneOffset = 0 },
            new DiatonicPitch() { Name = DiatonicPitchName.D, PitchOffset = 1, SemitoneOffset = 2 },
            new DiatonicPitch() { Name = DiatonicPitchName.E, PitchOffset = 2, SemitoneOffset = 4 },
            new DiatonicPitch() { Name = DiatonicPitchName.F, PitchOffset = 3, SemitoneOffset = 5 },
            new DiatonicPitch() { Name = DiatonicPitchName.G, PitchOffset = 4, SemitoneOffset = 7 },
            new DiatonicPitch() { Name = DiatonicPitchName.A, PitchOffset = 5, SemitoneOffset = 9 },
            new DiatonicPitch() { Name = DiatonicPitchName.B, PitchOffset = 6, SemitoneOffset = 11 }
        };

        public DiatonicPitch GetDiatonicPitch(DiatonicPitchName name)
        {
            return AllDiatonicPitches.First(p => p.Name == name);
        }

        public DiatonicPitch GetDiatonicPitch(int pitchOffset)
        {
            var pitchCount = AllDiatonicPitches.Count;
            return AllDiatonicPitches.First(p => p.PitchOffset == pitchOffset % pitchCount);
        }

        public IList<DiatonicPitch> GetAllDiatonicPitches()
        {
            return AllDiatonicPitches;
        }
    }
}
