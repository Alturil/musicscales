using System.Collections.Generic;
using System.Linq;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public class AccidentalFactory : IAccidentalFactory
    {
        private static List<Accidental> AllAccidentals => new List<Accidental>
        {
            new Accidental
            {
                Name = AccidentalName.DoubleFlat,
                SemitoneOffset = -2
            },
            new Accidental
            {
                Name = AccidentalName.Flat,
                SemitoneOffset = -1
            },
            new Accidental
            {
                Name = AccidentalName.Natural,
                SemitoneOffset = 0
            },
            new Accidental
            {
                Name = AccidentalName.Sharp,
                SemitoneOffset = 1
            },
            new Accidental
            {
                Name = AccidentalName.DoubleSharp,
                SemitoneOffset = 2
            }
        };

        public Accidental GetAccidental(AccidentalName name)
        {
            return AllAccidentals.First(a => a.Name == name);
        }

        public IList<Accidental> GetAllAccidentals()
        {
            return AllAccidentals;
        }
    }
}
