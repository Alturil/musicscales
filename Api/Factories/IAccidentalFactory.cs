using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public interface IAccidentalFactory
    {
        Accidental GetAccidental(AccidentalName name);
        IList<Accidental> GetAllAccidentals();
    }
}
