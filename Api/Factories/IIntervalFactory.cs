using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public interface IIntervalFactory
    {
        Interval GetInterval(IntervalSizeName name, IntervalQualityName quality);
        IList<IntervalCollection> GetAllIntervals();
    }
}
