using System.Collections.Generic;
using System.Linq;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Factories
{
    public class IntervalFactory : IIntervalFactory
    {
        private IList<IntervalCollection> allIntervals => new List<IntervalCollection>
        {
            new IntervalCollection
            {
                Name = IntervalSizeName.Unison,
                PitchOffset = 0,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = -1
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Perfect,
                        SemitoneOffset = 0
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 1
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Second,
                PitchOffset = 1,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 0
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Minor,
                        SemitoneOffset = 1
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Major,
                        SemitoneOffset = 2
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 3
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Third,
                PitchOffset = 2,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 2
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Minor,
                        SemitoneOffset = 3
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Major,
                        SemitoneOffset = 4
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 5
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Fourth,
                PitchOffset = 3,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 4
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Perfect,
                        SemitoneOffset = 5
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 6
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Fifth,
                PitchOffset = 4,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 6
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Perfect,
                        SemitoneOffset = 7
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 8
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Sixth,
                PitchOffset = 5,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 7
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Minor,
                        SemitoneOffset = 8
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Major,
                        SemitoneOffset = 9
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 10
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Seventh,
                PitchOffset = 6,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 9
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Minor,
                        SemitoneOffset = 10
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Major,
                        SemitoneOffset = 11
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 12
                    }
                }
            },
            new IntervalCollection
            {
                Name = IntervalSizeName.Octave,
                PitchOffset = 7,
                Quality = new List<IntervalQuality>
                {
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Diminished,
                        SemitoneOffset = 11
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Perfect,
                        SemitoneOffset = 12
                    },
                    new IntervalQuality
                    {
                        Name = IntervalQualityName.Augmented,
                        SemitoneOffset = 13
                    }
                }
            }
        };

        public Interval GetInterval(IntervalSizeName name, IntervalQualityName quality)
        {
            var interval = allIntervals.Where(i => i.Name == name).First();
            var intervalQuality = interval.Quality.Where(q => q.Name == quality).First();

            return new Interval
            {
                Name = interval.Name,
                Quality = intervalQuality.Name,
                PitchOffset = interval.PitchOffset,
                SemitoneOffset = intervalQuality.SemitoneOffset
            };
        }

        public IList<IntervalCollection> GetAllIntervals()
        {
            return allIntervals;
        }
    }
}
