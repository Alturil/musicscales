using System;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using musicscales.Api.Factories;
using musicscales.Api.Repositories;
using musicscales.Api.Services;

namespace musicscales
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                // options.JsonSerializerOptions.IgnoreNullValues = true;
            });

            services.AddControllers();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "1",
                    Title = "Musical Scales API",
                    Description = "An object oriented solution for musical scales",
                    Contact = new OpenApiContact
                    {
                        Name = "Jonatan Jaworski",
                        Email = "alturil@gmail.com",
                        Url = new Uri("https://gitlab.com/Alturil"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI. ?????????????
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // Repositories
            services.AddSingleton<IScaleRepository, ScaleRepository>();

            // Factories
            services.AddSingleton<IAccidentalFactory, AccidentalFactory>();
            services.AddSingleton<IDiatonicPitchFactory, DiatonicPitchFactory>();
            services.AddSingleton<IIntervalFactory, IntervalFactory>();

            // Services
            services.AddSingleton<IAccidentalService, AccidentalService>();
            services.AddSingleton<IPitchService, PitchService>();
            services.AddSingleton<IIntervalService, IntervalService>();
            services.AddSingleton<IScaleService, ScaleService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            // app.UseAuthorization();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Musical Scales API");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
