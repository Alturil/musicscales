using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using musicscales.Api.Models;

namespace musicscales.Api.Repositories
{
    public class ScaleRepository : IScaleRepository
    {
        private ILiteDatabase _liteDb = new LiteDatabase(@"Filename=.\MusicalScales.db");
        private ILiteCollection<Scale> collection;

        public ScaleRepository()
        {
            collection = _liteDb.GetCollection<Scale>("scales");
        }

        public Scale GetScale(Guid scaleGuid)
        {
            return collection.FindById(scaleGuid);
        }

        public IList<Scale> GetScale(string scaleName)
        {
            return collection.Find(s => s.Metadata.Names.Any(n => n == scaleName)).ToList();
        }

        public Scale GetScale(IList<Interval> intervals)
        {
            return collection.Find(s => s.Intervals == intervals).ToList().FirstOrDefault();
        }

        public IList<Scale> GetAllScales()
        {
            return new List<Scale>(collection.FindAll().Select(s => new Scale
            {
                Guid = s.Guid,
                Metadata = s.Metadata,
                Intervals = s.Intervals
            }).ToList());
        }

        public Guid SaveScale(Scale scale)
        {
            return collection.Insert(scale).AsGuid;
        }

        public bool DeleteScale(Guid scaleGuid)
        {
            return collection.Delete(scaleGuid);
        }

        public bool UpdateScale(Guid scaleGuid, Scale scale)
        {
            var existingScale = GetScale(scaleGuid);

            if (existingScale != null)
            {
                existingScale.Metadata = scale.Metadata;
                existingScale.Intervals = scale.Intervals;
                return collection.Update(existingScale);
            }
            return false;
        }

    }
}
