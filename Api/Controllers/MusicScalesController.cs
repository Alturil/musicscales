using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using musicscales.Api.Contracts;
using musicscales.Api.Contracts.Responses;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;
using musicscales.Api.Services;

namespace musicscales.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MusicScalesController : ControllerBase
    {
        private readonly ILogger<MusicScalesController> _logger;
        private readonly IPitchService _pitchService;
        private readonly IIntervalService _intervalService;
        private readonly IScaleService _scaleService;

        public MusicScalesController(
            ILogger<MusicScalesController> logger,
            IPitchService pitchService,
            IIntervalService intervalService,
            IScaleService scaleService)
        {
            _logger = logger;
            _pitchService = pitchService;
            _intervalService = intervalService;
            _scaleService = scaleService;
        }

        /// <summary>
        /// Gets the pitches of a scale, given a root pitch, its accidental, and the scale name
        /// </summary>
        /// <param name="scaleName">Name of the scale</param>
        /// <param name="rootPitchName">Diatonic root pitch name</param>
        /// <param name="accidentalName">Root pitch accidental (defaults to natural)</param>
        [HttpGet]
        [Route("/scale/pitches")]
        public IActionResult GetScalePitches([FromQuery]string scaleName,
                                            [FromQuery]DiatonicPitchName rootPitchName,
                                            [FromQuery]AccidentalName accidentalName = AccidentalName.Natural)
        {
            var rootPitch = _pitchService.GetPitch(rootPitchName, accidentalName);

            // No silly root pitches please
            if (rootPitch.Accidental == AccidentalName.DoubleFlat || rootPitch.Accidental == AccidentalName.DoubleSharp)
            {
                return new JsonResult(new { Message = "Double accidentals are not allowed as root notes in this endpoint." })
                {
                    StatusCode = 400
                };
            }

            var scale = _scaleService.GetScale(scaleName);

            if (scale.Count == 0)
            {
                return new JsonResult(new { Message = "Scale not found." })
                {
                    StatusCode = 404
                };
            }

            if (scale.Count > 1)
            {
                return new JsonResult(new { Message = "More than one scale found." })
                {
                    StatusCode = 409
                };
            }

            var scaleIntervals = scale.First().Intervals
                .Select(i => _intervalService.GetInterval(i.Name, i.Quality))
                .ToList();
            var pitches = _scaleService.GetScalePitches(rootPitch, scaleIntervals);

            return new JsonResult(
                new PitchCollectionContract
                {
                    Root = new PitchContract { Name = rootPitch.Name, Accidental = rootPitch.Accidental },
                    ScaleName = scaleName,
                    Pitches = pitches.Select(p => new PitchContract { Name = p.Name, Accidental = p.Accidental })
                                     .ToList()
                })
            {
                StatusCode = 200
            };

        }

        /// <summary>
        /// Gets all the available scales
        /// </summary>
        [HttpGet]
        [Route("/scale")]
        public IActionResult GetAllScales()
        {
            var getAllScalesResponse = _scaleService.GetAllScales();

            var scales = getAllScalesResponse.Select(MapToScaleContract).ToList();

            return new JsonResult(new ScaleCollectionResponseContract
            {
                Message = $"{scales.Count} scales found.",
                Scales = scales
            })
            {
                StatusCode = 200
            };

        }

        /// <summary>
        /// Gets a scale by Guid
        /// </summary>
        /// <param name="scaleGuid">Scale Guid</param>
        [HttpGet]
        [Route("/scale/{scaleGuid}")]
        public IActionResult GetScaleByGuid(Guid scaleGuid)
        {
            var getScaleResponse = _scaleService.GetScale(scaleGuid);

            return (getScaleResponse != null)
            ? new JsonResult(MapToScaleContract(getScaleResponse))
            {
                StatusCode = 200
            }
            : new JsonResult(new GuidResponseContract { Message = "Scale not found", Guid = scaleGuid })
            {
                StatusCode = 404
            };

        }


        /// <summary>
        /// Finds scales by name
        /// </summary>
        [HttpGet]
        [Route("/scale/search")]
        public IActionResult GetScaleByName([FromQuery]string scaleName)
        {
            var getScaleResponse = _scaleService.GetScale(scaleName);

            var scales = getScaleResponse.Select(MapToScaleContract).ToList();

            return new JsonResult(new ScaleCollectionResponseContract
            {
                Message = $"{scales.Count} scales found.",
                Scales = scales
            })
            {
                StatusCode = 200
            };
        }

        /// <summary>
        /// Creates a new scale
        /// </summary>
        /// <param name="scale">Scale</param>
        [HttpPost]
        [Route("/scale")]
        public IActionResult SaveScale([FromBody]ScaleContract scale)
        {
            var existingScale = _scaleService.GetScale(MapToIntervalCollection(scale.Intervals));

            if (existingScale != null)
            {
                return new JsonResult(new ScaleResponseContract
                {
                    Message = "A scale with these intervals already exists.",
                    Scale = MapToScaleContract(existingScale)
                })
                {
                    StatusCode = 409
                };
            }

            var savedScaleGuid = _scaleService.SaveScale(MapToScale(scale));

            return new JsonResult(new GuidResponseContract
            {
                Message = "New scale saved.",
                Guid = savedScaleGuid
            })
            {
                StatusCode = 201
            };
        }

        /// <summary>
        /// Updates an existing scale
        /// </summary>
        /// <param name="scaleGuid">Scale Guid</param>
        /// <param name="scale">Scale</param>
        [HttpPut]
        [Route("/scale/{scaleGuid}")]
        public IActionResult SaveScale(Guid scaleGuid, [FromBody]ScaleContract scale)
        {
            if (_scaleService.GetScale(scaleGuid) == null)
            {
                return new JsonResult(new GuidResponseContract
                {
                    Message = "Scale not found.",
                    Guid = scaleGuid
                })
                {
                    StatusCode = 404
                };
            }

            var scaleUpdated = _scaleService.UpdateScale(scaleGuid, MapToScale(scale));

            return new JsonResult(new GuidResponseContract
            {
                Message = "Scale successfully updated.",
                Guid = scaleGuid
            })
            {
                StatusCode = 200
            };
        }

        /// <summary>
        /// Deletes an existing scale
        /// </summary>
        /// <param name="scaleGuid">Scale Guid</param>
        [HttpDelete]
        [Route("/scale/{scaleGuid}")]
        public IActionResult DeleteScale(Guid scaleGuid)
        {
            if (_scaleService.GetScale(scaleGuid) == null)
            {
                return new JsonResult(new GuidResponseContract
                {
                    Message = "Scale not found.",
                    Guid = scaleGuid
                })
                {
                    StatusCode = 404
                };
            }

            var scaleDeleted = _scaleService.DeleteScale(scaleGuid);

            return new JsonResult(new GuidResponseContract
            {
                Message = "Scale successfully deleted.",
                Guid = scaleGuid
            })
            {
                StatusCode = 200
            };
        }

        private Scale MapToScale(ScaleContract scaleContract)
        {
            return new Scale
            {
                Guid = scaleContract.Guid,
                Metadata = new ScaleMetadata
                {
                    Names = scaleContract.Metadata.Names
                },
                Intervals = scaleContract.Intervals.Select(i => new Interval
                {
                    Name = i.Name,
                    Quality = i.Quality
                }).ToList()
            };
        }

        private ScaleContract MapToScaleContract(Scale scale)
        {
            return new ScaleContract
            {
                Guid = scale.Guid,
                Metadata = new ScaleMetadataContract
                {
                    Names = scale.Metadata.Names
                },
                Intervals = scale.Intervals.Select(i => new IntervalContract
                {
                    Name = i.Name,
                    Quality = i.Quality
                }).ToList()
            };
        }

        private IList<Interval> MapToIntervalCollection(IList<IntervalContract> intervals)
        {
            return intervals.Select(i => new Interval
            {
                Name = i.Name,
                Quality = i.Quality
            }).ToList();
        }

    }
}
