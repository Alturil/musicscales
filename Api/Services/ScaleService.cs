using System;
using System.Collections.Generic;
using System.Linq;
using musicscales.Api.Models;
using musicscales.Api.Repositories;

namespace musicscales.Api.Services
{
    public class ScaleService : IScaleService
    {
        private readonly IPitchService _pitchService;
        private readonly IScaleRepository _scaleRepository;
        public ScaleService(IPitchService pitchService, IScaleRepository scaleRepository)
        {
            _pitchService = pitchService;
            _scaleRepository = scaleRepository;
        }

        public IList<Pitch> GetScalePitches(Pitch rootPitch, IList<Interval> scaleIntervals)
        {
            var scalePitches = new List<Pitch> { rootPitch };

            scalePitches.AddRange(from Interval interval in scaleIntervals
                                  select _pitchService.GetPitch(scalePitches.Last(), interval));

            return scalePitches;
        }

        public Scale GetScale(Guid scaleGuid)
        {
            return _scaleRepository.GetScale(scaleGuid);
        }

        public IList<Scale> GetScale(string scaleName)
        {
            return _scaleRepository.GetScale(scaleName);
        }

        public Scale GetScale(IList<Interval> intervals)
        {
            return _scaleRepository.GetScale(intervals);
        }

        public IList<Scale> GetAllScales()
        {
            return _scaleRepository.GetAllScales();
        }

        public Guid SaveScale(Scale scale)
        {
            return _scaleRepository.SaveScale(scale);
        }

        public bool UpdateScale(Guid scaleGuid, Scale scale)
        {
            return _scaleRepository.UpdateScale(scaleGuid, scale);
        }

        public bool DeleteScale(Guid scaleGuid)
        {
            return _scaleRepository.DeleteScale(scaleGuid);
        }
    }
}
