using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public interface IIntervalService
    {
        Interval GetInterval(IntervalSizeName name, IntervalQualityName quality);
        IList<IntervalCollection> GetAllIntervals();
    }
}
