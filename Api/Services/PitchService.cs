using System.Collections.Generic;
using System.Linq;
using musicscales.Api.Factories;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public class PitchService : IPitchService
    {
        private readonly IDiatonicPitchFactory _pitchFactory;
        private readonly IAccidentalFactory _accidentalFactory;
        private readonly IList<Accidental> _allAccidentals;
        private const int TotalSemitones = 12;

        public PitchService(IDiatonicPitchFactory pitchFactory, IAccidentalFactory accidentalFactory)
        {
            _pitchFactory = pitchFactory;
            _accidentalFactory = accidentalFactory;
            _allAccidentals = accidentalFactory.GetAllAccidentals();
        }

        public IList<DiatonicPitch> GetAllDiatonicPitches()
        {
            return _pitchFactory.GetAllDiatonicPitches();
        }

        public Pitch GetPitch(DiatonicPitchName diatonicPitchName, AccidentalName accidentalName = AccidentalName.Natural)
        {
            var diatonicPitch = _pitchFactory.GetDiatonicPitch(diatonicPitchName);
            var accidental = _accidentalFactory.GetAccidental(accidentalName);

            return new Pitch
            {
                Name = diatonicPitch.Name,
                Accidental = accidental.Name,
                PitchOffset = diatonicPitch.PitchOffset,
                SemitoneOffset = diatonicPitch.SemitoneOffset + accidental.SemitoneOffset
            };
        }        

        public Pitch GetPitch(Pitch startingPitch, Interval interval)
        {
            var diatonicPitch = GetDiatonicPitch(startingPitch, interval);
            var accidental = GetAccidental(startingPitch, interval, diatonicPitch);

            return new Pitch
            {
                Name = diatonicPitch.Name,
                Accidental = accidental.Name,
                PitchOffset = diatonicPitch.PitchOffset,
                SemitoneOffset = diatonicPitch.SemitoneOffset + accidental.SemitoneOffset
            };
        }

        private DiatonicPitch GetDiatonicPitch(Pitch startingPitch, Interval interval)
        {
            return _pitchFactory.GetDiatonicPitch(startingPitch.PitchOffset + interval.PitchOffset);
        }

        private Accidental GetAccidental(Pitch startingPitch, Interval interval, DiatonicPitch targetPitch)
        {
            var resultingSemitoneOffset = startingPitch.SemitoneOffset + interval.SemitoneOffset - targetPitch.SemitoneOffset;            

            var accidentalSemitoneOffset = resultingSemitoneOffset;
            
            // Wrap around but allow for negative offsets
            if      (resultingSemitoneOffset < -2) { accidentalSemitoneOffset += TotalSemitones; }
            else if (resultingSemitoneOffset >  2) { accidentalSemitoneOffset -= TotalSemitones; }
            
            return _allAccidentals.First(a => a.SemitoneOffset == accidentalSemitoneOffset);
        }
    }
}