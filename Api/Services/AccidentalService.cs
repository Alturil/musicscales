using System.Collections.Generic;
using musicscales.Api.Factories;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public class AccidentalService : IAccidentalService
    {
        private readonly IAccidentalFactory _accidentalFactory;
        public AccidentalService(IAccidentalFactory accidentalFactory)
        {
            _accidentalFactory = accidentalFactory;
        }

        public Accidental GetAccidental(AccidentalName name)
        {
            return _accidentalFactory.GetAccidental(name);
        }

        public IList<Accidental> GetAllAccidentals()
        {
            return _accidentalFactory.GetAllAccidentals();
        }
    }
}
