using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public interface IPitchService
    {
        Pitch GetPitch(DiatonicPitchName name, AccidentalName accidental = AccidentalName.Natural);
        IList<DiatonicPitch> GetAllDiatonicPitches();
        Pitch GetPitch(Pitch startingPitch, Interval interval);
    }
}
