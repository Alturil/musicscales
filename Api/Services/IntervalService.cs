using System.Collections.Generic;
using musicscales.Api.Factories;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public class IntervalService : IIntervalService
    {
        private readonly IIntervalFactory _intervalFactory;

        public IntervalService(IIntervalFactory intervalFactory)
        {
            _intervalFactory = intervalFactory;
        }

        public Interval GetInterval(IntervalSizeName name, IntervalQualityName quality)
        {
            return _intervalFactory.GetInterval(name, quality);
        }

        public IList<IntervalCollection> GetAllIntervals()
        {
            return _intervalFactory.GetAllIntervals();
        }

    }
}
