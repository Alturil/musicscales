using System.Collections.Generic;
using musicscales.Api.Models;
using musicscales.Api.Models.Enums;

namespace musicscales.Api.Services
{
    public interface IAccidentalService
    {
        Accidental GetAccidental(AccidentalName name);
        IList<Accidental> GetAllAccidentals();
    }
}
