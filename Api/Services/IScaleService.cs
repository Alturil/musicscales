using System;
using System.Collections.Generic;
using musicscales.Api.Models;

namespace musicscales.Api.Services
{
    public interface IScaleService
    {
        IList<Pitch> GetScalePitches(Pitch startingPitch, IList<Interval> scaleIntervals);
        Scale GetScale(Guid scaleGuid);
        IList<Scale> GetScale(string scaleName);
        Scale GetScale(IList<Interval> intervals);
        IList<Scale> GetAllScales();
        Guid SaveScale(Scale scale);
        bool UpdateScale(Guid scaleGuid, Scale scale);
        bool DeleteScale(Guid scaleGuid);
    }
}
