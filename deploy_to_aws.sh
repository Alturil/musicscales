sudo apt-get update
sudo docker kill musical_scales
sudo docker rm -f musical_scales
sudo docker login registry.gitlab.com -u $1 -p $2
sudo docker pull registry.gitlab.com/alturil/musicscales:latest
sudo docker run -d -p 80:80 --name musical_scales registry.gitlab.com/alturil/musicscales:latest