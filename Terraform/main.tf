# secret_key and access_key are inferred from env variables
# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
provider "aws" {
  region = "us-east-1"
}

resource "aws_key_pair" "jonatan_tonal" {
  key_name = "aws-ec2-musicalscales"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjz/GOyuyzvIlYtdDFNFfWaJnzJocdvSQSxZZDuRpAs3bVyfJNZ36vBAa+VkRnPfkYZMMuVBaCvdnLqex0TMn53oRsYp6+0WJJtN19Z59MI4aQrut/yKQuaVUgwnGHz3yaSjcWTtpLKc2+KSe1wzT0hT4+g71+d0uvxTBtwr939Ljw0hszGW/q5EDtEbc5Rwd8ZDxRXj81biWYPbFY3U0BPsYez5dgxnDgc7PNdsrC4uwJlkvO7YjC077MenALygH1x3fjaphdqYRCMAV+KHwUlcySYFQvrb/O3PVW+scqkRADGiRJQRpvm/34HRmAjXhiEm2Q3Vdih7JlC9cWZJXp Jonatan Jaworski@TONAL"
}

resource "aws_instance" "web" {
  ami           = "ami-07ebfd5b3428b6f4d"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.jonatan_tonal.key_name
  vpc_security_group_ids = [
      "sg-01feee524020c9368"
  ]  
  tags = {
    Name = "MusicScalesWebServer"
  }
  
  provisioner "file" {
    source      = "bootstrap.sh"
    destination = "/tmp/bootstrap.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/bootstrap.sh",
      "sudo /tmp/bootstrap.sh",
    ]
  }
}