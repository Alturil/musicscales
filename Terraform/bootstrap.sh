#!/bin/bash
# Install Docker - see https://phoenixnap.com/kb/how-to-install-docker-on-ubuntu-18-04
sudo apt-get update
sudo apt-get remove docker docker-engine docker.io
sudo apt --yes install docker.io
sudo systemctl start docker
sudo systemctl enable docker