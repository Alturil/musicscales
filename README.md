# Musical Scales

## Introduction

While there are many good solutions out there that show how to calculate musical scales, all of them approach the problem from an algorithmic perspective, and that leads to some shortfalls in terms of music theory, the most obvious ones being:

- Enharmonics are often ignored, so the sonic output may be correct, but the symbols are not.
- Only tones and semitones are allowed, leaving aside scales with other intervals, such as pentatonic scales.
- Only scales with seven pitches are allowed, again leaving out scales with less ore more pitches like the diminished scale.
- For that same reason, scales spanning more than one octave are also dismissed.

My project is meant to represent scales using an object-oriented approach instead, where the issues listed above are not a problem but the essence of how scales are modeled.

This is a work in progress and I'm planning to publish a few articles on all the moving parts that this project encompasses, so watch this space!

# Content of this project

MusicalScales is a RESTful API that allows creating, updating, deleting and reading musical scales, as well as returning the exact pitches for a particular scale provided a root note (more on that soon). The project contains:

- The git repository
- A pipeline with the following stages:
    - Build and run local tests (currently only [integration](https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-3.1))
    - Build a docker image and publish to the GitLab Registry
    - Deploy the container to an EC2 instance in AWS and run it
    - Run API tests against AWS and export an HTML report

The API is currently live at http://www.musicalscales.net/, but can be run locally using the `dotnet` runtime sdk or via Docker.

# Next steps

- Improve the testing harness, and incorporate to the pipeline (unit tests, more integration tests, run api tests against the new image instead of the deployed webserver)
- Incorporate Terrform to the pipeline to provision the webservers
- Replace LiteDb with DynamoDb in AWS
- Write articles on what I've got so far