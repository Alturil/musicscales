FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY ./Api ./app/Api

RUN dotnet restore ./app/Api/musicscales.csproj

RUN dotnet publish ./app/Api -c Release -o out

# Run
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1

WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 80
ENTRYPOINT ["dotnet", "musicscales.dll" ]